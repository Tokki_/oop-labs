//lab2 - �������� � �������

#include <iostream>
#include <string>

class Ratio
{
public:
	Ratio(int numer, int dumer) : _numer(numer), _dumer(dumer) {} //�����������

	Ratio() = default;
	Ratio(const Ratio& ratio) = default;
	~Ratio() = default;

	Ratio(const std::string& ratio)
	{
		ConvertToNumbers(ratio);
	}

	//����� �������� � ����������� ��� ������
	int getNumer() const { return _numer; }
	int getDumer() const { return _dumer; }

	void reduction() //���������� �����
	{
		int gcd_value = gcd(_numer, _dumer);
		_numer /= gcd_value;
		_dumer /= gcd_value;
	}

	void reverse() //�������� �����
	{
		std::swap(_numer, _dumer);
	}

	double decimal() //���������� �����
	{
		double a = _numer * 1.0 / _dumer;
		return a;
	}

	friend std::istream& operator>>(std::istream& in, Ratio& ratio); //��������������� ���������� �����
	friend std::ostream& operator<<(std::ostream& out, const Ratio& ratio); //� ������

	Ratio& operator=(const Ratio& ratio) = default; 
	
	Ratio operator+(const Ratio& ratio) //�������������� +
	{
		Ratio tmp = *this;
		tmp._numer = _numer * ratio._dumer + _dumer * ratio._numer;
		tmp._dumer = _dumer * ratio._dumer;
		tmp.reduction();
		return tmp;
	}

	Ratio operator-(const Ratio& ratio) //�������������� -
	{
		Ratio tmp = *this;
		tmp._numer = _numer * ratio._dumer - _dumer * ratio._numer;
		tmp._dumer = _dumer * ratio._dumer;
		tmp.reduction();
		return tmp;
	}

	Ratio operator/(const Ratio& ratio) //�������������� /
	{
		Ratio tmp= *this;
		tmp._numer = _numer * ratio._dumer;
		tmp._dumer = _dumer * ratio._numer;
		tmp.reduction();
		return tmp;
	}

	Ratio operator*(const Ratio& ratio) //�������������� *
	{
		Ratio tmp = *this;
		tmp._numer = _numer * ratio._numer;
		tmp._dumer = _dumer * ratio._dumer;
		tmp.reduction();
		return tmp;
	}

private:
	int _numer;
	int _dumer;

	void ConvertToNumbers(const std::string& value)
	{
		int pos = value.find('/'); //��������� ������� ����� �����
		std::string numerStr = value.substr(0, pos); //������ �� �����
		std::string dumerStr = value.substr(pos + 1, value.length() - pos); //������ ����� �����

		_numer = std::stoi(numerStr); //��������� (������ ���������� � �����)
		_dumer = std::stoi(dumerStr); //�����������
	}

	int gcd(int a, int b) //���
	{
		if (b == 0) return a;
		return gcd(b, a % b);
	}
};

std::istream& operator>>(std::istream& in, Ratio& ratio)
{
	std::string value;
	in >> value;
	ratio.ConvertToNumbers(value);
	return in;
}

std::ostream& operator<<(std::ostream& out, const Ratio& ratio)
{
	out << ratio._numer << "/" << ratio._dumer;
	return out;
}

int main()
{
	setlocale(LC_ALL, "Rus");

	Ratio r1;
	Ratio r2;

	int demo = 1;

	std::cout << "������� ��� ���������� ����� ����� ������" << std::endl;
	std::cin >> r1 >> r2;

	while (demo != 0)
	{
		std::cout << "������� 0, ����� �����" << std::endl;
		std::cout << "������� 1, ����� �������" << std::endl;
		std::cout << "������� 2, ����� �������" << std::endl;
		std::cout << "������� 3, ����� ��������" << std::endl;
		std::cout << "������� 4, ����� ���������" << std::endl;
		std::cout << "������� 5, ����� ����� �������� �����" << std::endl;
		std::cout << "������� 6, ����� ����� ���������� �����" << std::endl;

		std::cin >> demo;

		switch (demo)
		{
			case 1: std::cout << r1 + r2 << std::endl;
				break;
			case 2: std::cout << r1 - r2 << std::endl;
				break;
			case 3: std::cout << r1 * r2 << std::endl;
				break;
			case 4: std::cout << r1 / r2 << std::endl;
				break;
			case 5: 
				r1.reverse();
				r2.reverse();
				std::cout << r1 << " " << r2 << std::endl;
				break;
			case 6: 
				std::cout << r1.decimal() << " " << r2.decimal();
			default:
				std::cout << " " << std::endl;
				break;
		}
	};

	return 0;
}