#include <vector>
#include <iostream>

template <typename T>
class Array3d {
public:
    Array3d(int dim0, int dim1, int dim2);

    T& operator()(int i, int j, int k);

    std::vector<T> GetValues0(int i);
    std::vector<T> GetValues1(int j);
    std::vector<T> GetValues2(int k);
    std::vector<T> GetValues01(int i, int j);
    std::vector<T> GetValues02(int i, int k);
    std::vector<T> GetValues12(int j, int k);

    void SetValues0(int i, std::vector<T> values);
    void SetValues1(int j, std::vector<T> values);
    void SetValues2(int k, std::vector<T> values);
    void SetValues01(int i, int j, std::vector<T> values);
    void SetValues02(int i, int k, std::vector<T> values);
    void SetValues12(int j, int k, std::vector<T> values);

    void printArray();

    static Array3d<T> Fill(int dim0, int dim1, int dim2, T value);

protected:
    int dim0_, dim1_, dim2_;
    std::vector<T> data_;
};