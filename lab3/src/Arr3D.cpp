#include "../inc/Arr3d.h"

template <typename T>
Array3d<T>::Array3d(int dim0, int dim1, int dim2)
        : dim0_(dim0), dim1_(dim1), dim2_(dim2), data_(dim0 * dim1 * dim2) {}


template <typename T>
T& Array3d<T>::operator()(int i, int j, int k) {
    return data_[i * dim1_ * dim2_ + j * dim2_ + k];
}


template <typename T>
std::vector<T> Array3d<T>::GetValues0(int i) {
    std::vector<T> result;
    for (int j = 0; j < dim1_; ++j) {
        for (int k = 0; k < dim2_; ++k) {
            result.push_back((*this)(i, j, k));
        }
    }
    return result;
}


template <typename T>
std::vector<T> Array3d<T>::GetValues1(int j) {
    std::vector<T> result;
    for (int i = 0; i < dim0_; ++i) {
        for (int k = 0; k < dim2_; ++k) {
            result.push_back((*this)(i, j, k));
        }
    }
    return result;
}


template <typename T>
std::vector<T> Array3d<T>::GetValues2(int k) {
    std::vector<T> result;
    for (int i = 0; i < dim0_; ++i) {
        for (int j = 0; j < dim1_; ++j) {
            result.push_back((*this)(i, j, k));
        }
    }
    return result;
}


template <typename T>
std::vector<T> Array3d<T>::GetValues01(int i, int j) {
    std::vector<T> result;
    for (int k = 0; k < dim2_; ++k) {
        result.push_back((*this)(i, j, k));
    }
    return result;
}


template <typename T>
std::vector<T> Array3d<T>::GetValues02(int i, int k) {
    std::vector<T> result;
    for (int j = 0; j < dim1_; ++j) {
        result.push_back((*this)(i, j, k));
    }
    return result;
}


template <typename T>
std::vector<T> Array3d<T>::GetValues12(int j, int k) {
    std::vector<T> result;
    for (int i = 0; i < dim0_; ++i) {
        result.push_back((*this)(i, j, k));
    }
    return result;
}


template <typename T>
void Array3d<T>::SetValues0(int i, std::vector<T> values) {
    int index = 0;
    for (int j = 0; j < dim1_; ++j) {
        for (int k = 0; k < dim2_; ++k) {
            (*this)(i, j, k) = values[index++];
        }
    }
}


template <typename T>
void Array3d<T>::SetValues1(int j, std::vector<T> values) {
    int index = 0;
    for (int i = 0; i < dim0_; ++i) {
        for (int k = 0; k < dim2_; ++k) {
            (*this)(i, j, k) = values[index++];
        }
    }
}


template <typename T>
void Array3d<T>::SetValues2(int k, std::vector<T> values) {
    int index = 0;
    for (int i = 0; i < dim0_; ++i) {
        for (int j = 0; j < dim1_; ++j) {
            (*this)(i, j, k) = values[index++];
        }
    }
}


template <typename T>
void Array3d<T>::SetValues01(int i, int j, std::vector<T> values) {
    int index = 0;
    for (int k = 0; k < dim2_; ++k) {
        (*this)(i, j, k) = values[index++];
    }
}


template <typename T>
void Array3d<T>::SetValues02(int i, int k, std::vector<T> values) {
    int index = 0;
    for (int j = 0; j < dim1_; ++j) {
        (*this)(i, j, k) = values[index++];
    }
}


template <typename T>
void Array3d<T>::SetValues12(int j, int k, std::vector<T> values) {
    int index = 0;
    for (int i = 0; i < dim0_; ++i) {
        (*this)(i, j, k) = values[index++];
    }
}


template <typename T>
Array3d<T> Array3d<T>::Fill(int dim0, int dim1, int dim2, T value) {
    Array3d<T> array(dim0, dim1, dim2);
    for (int i = 0; i < dim0; ++i) {
        for (int j = 0; j < dim1; ++j) {
            for (int k = 0; k < dim2; ++k) {
                array(i, j, k) = value;
            }
        }
    }
    return array;
}


template <typename T>
void Array3d<T>::printArray() {
    std::cout << "Array output:" << std::endl;
    for (int i = 0; i < dim0_; ++i) {
        for (int j = 0; j < dim1_; ++j) {
            for (int k = 0; k < dim2_; ++k) {
                std::cout << (*this)(i, j, k) << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }
    std::cout << "End of array output" << std::endl;
    std::cout << std::endl;
}

template class Array3d<int>;
