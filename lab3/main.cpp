#include <iostream>
#include "inc/Arr3d.h"

int main() {
    Array3d<int> arr(2, 2, 2);
    arr.printArray();


    arr(0, 0, 0) = 35;
    arr(0, 1, 0) = 91;
    std::cout << arr(0, 0, 0) << " " << arr(0, 1, 0) << std::endl;
    arr.printArray();


    std::vector<int> values = {1, 2, 3, 4};

    arr.SetValues0(0, values);
    std::vector<int> values0 = arr.GetValues0(0);
    for (int &val: values0) {
        std::cout << val << " ";
    }
    std::cout << std::endl;

    arr.printArray();
    std::cout << std::endl;


    values = {5, 6, 7, 8};
    arr.SetValues1(0, values);
    std::vector<int> values1 = arr.GetValues1(0);
    for (int &val: values1) {
        std::cout << val << " ";
    }
    std::cout << std::endl;


    values = {9, 10, 11, 12};
    arr.SetValues2(0, values);
    std::vector<int> values2 = arr.GetValues2(0);
    for (int &val: values2) {
        std::cout << val << " ";
    }
    std::cout << std::endl;


    values = {13, 14};
    arr.SetValues01(0, 0, values);
    std::vector<int> values3 = arr.GetValues01(0, 0);
    for (int &val: values3) {
        std::cout << val << " ";
    }
    std::cout << std::endl;


    values = {15, 16};
    arr.SetValues02(0, 0, values);
    std::vector<int> values4 = arr.GetValues02(0, 0);
    for (int &val: values4) {
        std::cout << val << " ";
    }
    std::cout << std::endl;


    values = {17, 18};
    arr.SetValues12(0, 0, values);
    std::vector<int> values5 = arr.GetValues12(0, 0);
    for (int &val: values5) {
        std::cout << val << " ";
    }
    std::cout << std::endl;


    Array3d<int> filledArray = Array3d<int>::Fill(2, 2, 2, 82);
    filledArray.printArray();


    Array3d<int> filledArray1 = Array3d<int>::Fill(2, 2, 2, 1);
    filledArray1.printArray();

    return 0;
}