#include <functional>
#include <map>
#include <iostream>
#include <stack>
#include "workFlow.h"
#include <thread>


class Keyboard{

public:
    Keyboard(WorkFlow workFlow) : workFlow(workFlow) {};
    void assignKey(const std::string& key, const std::function<void()>& action, const std::function<void()>& undoAction = []() {});
    void pressKey(const std::string& key);
    void undo();
    bool isKey(const std::string& key) const;


private:
    WorkFlow workFlow;
    std::map<std::string, std::pair< std::function<void()>, std::function<void()>>> keymap;
    std::stack<std::string> werePressed;
};
