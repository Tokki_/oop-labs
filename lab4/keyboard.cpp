#include "keyboard.h"

//проверка нажатой клавиши (есть ли она в коллекции)
void Keyboard::pressKey(const std::string& key){
    if (!isKey(key)){
        std::cout << std::endl << "there is no such key" << std::endl;
        return;
    }

    workFlow.addKeyPressed(key);
    werePressed.push(key);
    keymap[key].first();

    std::this_thread::sleep_for(std::chrono::milliseconds{1000});
}


void Keyboard::undo(){
    if (werePressed.empty()) return;

    const std::string key = werePressed.top();
    workFlow.addUndo(key);
    keymap[key].second();
    werePressed.pop();

    std::this_thread::sleep_for(std::chrono::milliseconds{1000});
}


//добавление клавиши в коллекцию
void Keyboard::assignKey(const std::string& key, const std::function<void()>& action,
                      const std::function<void()>& undoAction){
    keymap[key] = { action, undoAction };
}

//проверка на наличие клавиши в коллекции
bool Keyboard::isKey(const std::string& key) const{
    return keymap.count(key) > 0;
}
