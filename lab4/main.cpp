//lab4
#include "keyboard.h"


int main(){
    WorkFlow workFlow;
    Keyboard keyboard = *new Keyboard(workFlow);

    keyboard.assignKey("a", []() { std::cout << "the action of the -- a -- key has been completed" << std::endl; },[]() { std::cout << "the action of the -- a -- key has been canceled" << std::endl; });
    keyboard.assignKey("ctrl+c", []() { std::cout << "action -- ctrl+c -- is completed" << std::endl; },[]() { std::cout << "action -- ctrl+c -- has been canceled" << std::endl; });
    keyboard.assignKey("ctrl+v", []() { std::cout << "action -- ctrl+v -- is completed" << std::endl; },[]() { std::cout << "action -- ctrl+v -- has been canceled" << std::endl; });


    keyboard.pressKey("a");
    keyboard.pressKey("ctrl+c");
    keyboard.pressKey("z");
    keyboard.pressKey("ctrl+v");

    std::cout << "----------------------------------------------" << std::endl;

    keyboard.assignKey("a", []() { std::cout << "-- NEW a --" << std::endl; },[]() { std::cout << "-- UNDO NEW a --" << std::endl; });
    keyboard.pressKey("a");

    std::cout << "----------------------------------------------" << std::endl;

    keyboard.undo();
    keyboard.undo();


    return 0;
}
