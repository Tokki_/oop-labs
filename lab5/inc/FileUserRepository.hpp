#pragma once
#include "IUserRepository.hpp"

namespace aki
{
	class FileUserRepository : aki::IUserRepository
	{
	public:
		FileUserRepository();

		virtual aki::User getById(int id) override;
		virtual aki::User getByLogin(const std::string& login) override;

		virtual std::vector<aki::User> Get() const override;
		virtual void add(aki::User item) override;
		virtual void remove(aki::User item) override;
		virtual void update() override;

		virtual aki::User checkLogin();
		virtual int createNewId();

	protected:
		std::vector<aki::User> _users;
	};
}
