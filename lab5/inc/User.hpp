#pragma once
#include <string>

namespace aki
{
	class User
	{
	public:
		int id;
		std::string name;
		std::string login;
		std::string password;
	};
}
