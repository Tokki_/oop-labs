#pragma once
#include "IUserManager.hpp"
#include "FileUserRepository.hpp"

namespace aki
{
	class FileUserManager : aki::IUserManager
	{
	public:
		FileUserManager();

		virtual void signIn(aki::User user) override;
		virtual void signOut() override;
		virtual bool isAuthorized() override;

		virtual bool isRegistered(aki::User user) override;
		virtual void signUp(std::string name, std::string login, std::string password) override;

	private:
		aki::User _curUser;
		aki::FileUserRepository _userRepository;
	};
}
