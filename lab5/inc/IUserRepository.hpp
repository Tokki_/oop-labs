#pragma once

namespace aki
{
	class IUserRepository : aki::IDataRepository<aki::User>
	{
	public:
		virtual aki::User getById(int id) = 0;
		virtual aki::User getByLogin(const std::string& name) = 0;
	};
}
