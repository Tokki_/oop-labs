#include "FileUserManager.hpp"
#include "FileUserRepository.hpp"
#include <iostream>

int main()
{
	aki::FileUserManager manager;
	aki::FileUserRepository repository;

	std::string userName = "Alina";

	int choice = 1;

	aki::User user = repository.checkLogin();

	if (!user.login.empty())
	{
		manager.signIn(user);
		std::cout << "Authorization was successful. Choose: \n0 - log out; 2 - add a new user; 1 - log in from another account" << std::endl;
	}

	std::string login, password, name;

	while (choice != 0)
	{
		if (!manager.isAuthorized())
		{
			std::cout << "To log in - enter your username and password (1) or register (2)" << std::endl;
		}

		std::cin >> choice;

		switch (choice)
		{
		case 1:
			std::cout << "Enter your username:" << std::endl;
			std::cin >> login;
			std::cout << "Enter the password:" << std::endl;
			std::cin >> password;

			user = repository.getByLogin(login);
			if (!user.login.empty() && user.password == password)
			{
				manager.signIn(user);
				std::cout << "Authorization was successful. Choose: \n0 - log out; 2 - add a new user; 1 - log in from another account" << std::endl;
			}

			else
			{
				std::cout << "Incorrect login or password is entered" << std::endl;
			}
			break;

		case 2:
			std::cout << "Enter your username:" << std::endl;
			std::cin >> login;
			std::cout << "Enter the password:" << std::endl;
			std::cin >> password;
			std::cout << "Enter a name:" << std::endl;
			std::cin >> name;

			manager.signUp(name, login, password);
			std::cout << "Registration was successful. Choose: \n2 - add a new user, 1 - log in from another account" << std::endl;
			break;

		default:
			break;
		}
	}

	return 0;
}
