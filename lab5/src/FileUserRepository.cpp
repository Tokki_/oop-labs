#include "FileUserRepository.hpp"
#include <iostream>
#include <fstream>

namespace ul
{
	FileUserRepository::FileUserRepository()
	{
		_users.clear();
		std::ifstream in("users.txt");

		if (in.is_open())
		{
			ul::User user;

			while (in >> user.id >> user.name >> user.login >> user.password)
			{
				_users.push_back(user);
			}
		}

		in.close();
	}

	ul::User FileUserRepository::getById(int id)
	{
		ul::User user;

		for (const ul::User& userF : _users)
		{
			if (userF.id == id)
			{
				user = userF;
				break;
			}
		}
		return user;
	}

	ul::User FileUserRepository::getByLogin(const std::string& login)
	{
		ul::User user;

		for (const ul::User& userF : _users)
		{
			if (userF.login == login)
			{
				user = userF;
				return user;
			}
		}
		return user;
	}

	std::vector<ul::User> FileUserRepository::Get() const { return _users; }

	void FileUserRepository::add(ul::User user)
	{
		_users.push_back(user);
		update();
	}

	void FileUserRepository::remove(ul::User user)
	{
		auto del = _users.begin();

		while (del != _users.end())
		{
			if (del->id == user.id)
			{
				_users.erase(del);
				break;
			}
			else
			{
				++del;
			}
		}
		update();
	}

	ul::User FileUserRepository::checkLogin()
	{
		std::ifstream in("lastAuthorized.txt");
		ul::User user;

		if (in.is_open())
		{
			in >> user.id >> user.name >> user.login >> user.password;
			in.close();
		}
		return user;
	}

	void FileUserRepository::update()
	{
		std::ofstream out("users.txt");

		if (out.is_open())
		{
			for (auto const& user : _users)
			{
				out << user.id << " " << user.name << " " << user.login << " " << user.password;
			}
		}
		out.close();
	}

	int FileUserRepository::createNewId()
	{
		return _users.size() + 1;
	}
}