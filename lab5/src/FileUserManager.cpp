#include "FileUserManager.hpp"

#include <iostream>
#include <fstream>

namespace aki
{
	FileUserRepository::FileUserRepository()
	{
		_users.clear();
		std::ifstream in("users.txt");

		if (in.is_open())
		{
			aki::User user;

			while (in >> user.id >> user.name >> user.login >> user.password)
			{
				_users.push_back(user);
			}
		}

		in.close();
	}

	aki::User FileUserRepository::getById(int id)
	{
		aki::User user;

		for (const aki::User& userF : _users)
		{
			if (userF.id == id)
			{
				user = userF;
				break;
			}
		}

		return user;
	}

	aki::User FileUserRepository::getByLogin(const std::string& login)
	{
		aki::User user;

		for (const aki::User& userF : _users)
		{
			if (userF.login == login)
			{
				user = userF;
				return user;
			}
		}

		return user;
	}

	std::vector<aki::User> FileUserRepository::Get() const { return _users; }

	void FileUserRepository::add(aki::User item)
	{
		_users.push_back(item);
		update();
	}

	void FileUserRepository::remove(aki::User item)
	{
		auto it = _users.begin();

		while (it != _users.end())
		{
			if (it->id == item.id)
			{
				_users.erase(it);
				break;
			}

			else
			{
				++it;
			}
		}

		update();
	}

	aki::User FileUserRepository::checkLogin()
	{
		std::ifstream in("lastAuthorized.txt");
		aki::User user;

		if (in.is_open())
		{
			in >> user.id >> user.name >> user.login >> user.password;
			in.close();
		}

		return user;
	}

	void FileUserRepository::update()
	{
		std::ofstream out("users.txt");

		if (out.is_open())
		{
			for (auto const& user : _users)
			{
				out << user.id << " " << user.name << " " << user.login << " " << user.password;
			}
		}

		out.close();
	}

	int FileUserRepository::createNewId()
	{
		return _users.size() + 1;
	}
}
